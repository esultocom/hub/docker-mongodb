# MongoDB dockerized custom image

[MongoDB](https://www.mongodb.com) Docker image extended with extra features

### This project include code from these projects

[smartsdk mongo-rs-controller-swarm](https://github.com/smartsdk/mongo-rs-controller-swarm)

### Please find LICENSE link for MongoDB below

[MongoDB license](https://www.mongodb.com/licensing/server-side-public-license)